import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

def facility = 'Hongkong CURA Healthcare Center'

String visit_date = '18/01/2024'

String comment = 'this is my comment'

WebUI.callTestCase(findTestCase('Block Test Case/block-login valid'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByValue(findTestObject('Object Spy/Appointment/drop_facility'), facility, false)

WebUI.click(findTestObject('Object Spy/Appointment/chk_hospital_readmission'))

WebUI.click(findTestObject('Object Spy/Appointment/radio_program_Medicaid'))

WebUI.setText(findTestObject('Object Spy/Appointment/inpt_visit_date'), visit_date)

WebUI.setText(findTestObject('Object Spy/Appointment/inpt_comment'), comment)

WebUI.click(findTestObject('Object Spy/Appointment/btn_Book Appointment'))

WebUI.waitForElementVisible(findTestObject('Object Spy/Appointment Confirmation/lbl_Appointment Confirmation'), 0)

WebUI.verifyElementVisible(findTestObject('Object Spy/Appointment Confirmation/lbl_Appointment Confirmation'))

WebUI.verifyElementVisible(findTestObject('Object Spy/Appointment Confirmation/lbl_Please be informed that your appointment'))

WebUI.comment('Verify submitted appointment data')

WebUI.verifyElementText(findTestObject('Object Spy/Appointment Confirmation/lbl_facility'), facility)

WebUI.verifyElementVisible(findTestObject('Object Spy/Appointment Confirmation/lbl_hospital_readmission'))

WebUI.verifyElementVisible(findTestObject('Object Spy/Appointment Confirmation/lbl_program'))

WebUI.verifyElementText(findTestObject('Object Spy/Appointment Confirmation/lbl_visit_date'), visit_date)

WebUI.verifyElementText(findTestObject('Object Spy/Appointment Confirmation/lbl_comment'), comment)

WebUI.click(findTestObject('Object Spy/Appointment Confirmation/btn_Go to Homepage'))

WebUI.waitForElementVisible(findTestObject('Object Spy/Homepage/lbl_CURA Healthcare Service'), 0)

WebUI.closeBrowser()

/*
copy xpath: //*[@id="header-main-wrapper"]/div[2]/div[5]/button[1]
copy full xpath: /html/body/div[1]/div/div[1]/div/div/div[2]/div[5]/button[1]


*/

