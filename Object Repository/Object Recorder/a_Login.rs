<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Login</name>
   <tag></tag>
   <elementGuidId>6535eff0-5875-4800-a4e1-7ca0b6a1562c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li:nth-of-type(3) > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//a[@onclick=&quot;$('#menu-close').click();&quot;])[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>650f53ae-99d5-4e2a-ae08-84f29feeef2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>profile.php#login</value>
      <webElementGuid>2e4b766b-e14d-4fed-be64-108e3b03c988</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>$('#menu-close').click();</value>
      <webElementGuid>98700afb-406d-4c9b-80d0-5eebbf64acd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Login</value>
      <webElementGuid>70306bb0-6c14-4de3-9819-d52772dacdf6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sidebar-wrapper&quot;)/ul[@class=&quot;sidebar-nav&quot;]/li[3]/a[1]</value>
      <webElementGuid>58c5e652-3abf-4e1d-af93-0325eb3e9b57</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//a[@onclick=&quot;$('#menu-close').click();&quot;])[3]</value>
      <webElementGuid>d8afbe43-cea4-416e-9dbf-854b2b9412ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//nav[@id='sidebar-wrapper']/ul/li[3]/a</value>
      <webElementGuid>3b22f42b-8fbd-4e59-b647-820b260d5462</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Login')]</value>
      <webElementGuid>e1b17e36-3c14-4ecc-ae7a-0dfe7be96a71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'profile.php#login')]</value>
      <webElementGuid>632d9121-f101-4c01-93f8-831054426b1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/a</value>
      <webElementGuid>3383d92d-fc1f-40e8-8256-51a6b2479d6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'profile.php#login' and (text() = 'Login' or . = 'Login')]</value>
      <webElementGuid>06b24474-7809-4837-b38e-238db4de33d3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
