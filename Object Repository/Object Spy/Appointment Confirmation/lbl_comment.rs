<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_comment</name>
   <tag></tag>
   <elementGuidId>15ef60bf-e3f1-4729-8938-033dd46b6eef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#comment</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//p[@id='comment']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>cde04d19-7515-4392-bc31-53668f296a4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>comment</value>
      <webElementGuid>b6006f5e-976d-412d-b4d5-c67772cf8556</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>this is my comment</value>
      <webElementGuid>cf595f1f-4ab8-4ffe-bfe3-f216fd50ec5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;comment&quot;)</value>
      <webElementGuid>471ae459-47bf-4b0a-9aa6-bf450981bc96</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//p[@id='comment']</value>
      <webElementGuid>1d9d59e3-2d6e-43a2-aca9-372b394b6727</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='summary']/div/div/div[6]/div[2]/p</value>
      <webElementGuid>efd182e6-0d3a-454c-ba1b-873a7b8ff36b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div[2]/p</value>
      <webElementGuid>26ef818d-13a8-49ae-a3c4-e36e9053eaa6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[@id = 'comment' and (text() = 'this is my comment' or . = 'this is my comment')]</value>
      <webElementGuid>8534d9d0-6650-41fb-a180-2b765f00b2e8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
